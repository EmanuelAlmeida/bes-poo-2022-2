package br.ucsal.bes.poo20222.aula02;

import java.time.LocalDate;

// Tipo de dado composto heterogêneo

public class Aluno {

	// Atributos
	
	Integer matricula;
	
	String nome;
	
	String email;
	
	LocalDate dataNascimento;
	
	String situacao;
	
	// Métodos
	
	void matricular() {
	}
	
	void cancelarMatricula() {
	}
	
	void atualizarEmail() {
	}
	
}
