package br.ucsal.bes.poo20222.aula12;

public class Exemplo {

	public static void main(String[] args) {

		ContaCorrente contaCorrente1 = new ContaCorrente("Claudio Neiva");
		ContaCorrenteEspecial contaCorrenteEspecia1 = new ContaCorrenteEspecial("Joaquim da Silva", 2000);

		contaCorrente1.depositar(1000);
		contaCorrente1.depositar(500);

		contaCorrenteEspecia1.depositar(300);

		System.out.println(contaCorrente1.getSaldo());
		System.out.println(contaCorrenteEspecia1.getSaldo());
		System.out.println();

		executarSaque(contaCorrente1, 700);
		executarSaque(contaCorrente1, 3000);

		executarSaque(contaCorrenteEspecia1, 900);
		
		executarSaque(null, 10);

	}

	private static void executarSaque(ContaCorrente contaCorrente, double valorSaque) {
//		System.out.println("Mensagem: " + ContaCorrente.retornarMensagem());
		try {
			contaCorrente.sacar(valorSaque);
			System.out.println("Saque realizado com sucesso.");
		} catch (NegocioException e) {
			System.out.println(e.getMessage());
		}
		System.out.println(contaCorrente.getSaldo());
		System.out.println();
	}

}
