package br.ucsal.bes.poo20222.aula12;

public class ContaCorrenteEspecial extends ContaCorrente {

	private double valorLimite;

	public ContaCorrenteEspecial(String nomeCorrentista, double valorLimite) {
		super(nomeCorrentista);
		this.valorLimite = valorLimite;
	}
	
	public static String retornarMensagem() {
		return "sou especial";
	}

	@Override
	public void sacar(double valor) throws NegocioException {
		if(valor > saldo + valorLimite) {
			throw new NegocioException(MENS_SALDO_INSUFICIENTE);
		}
		saldo -= valor;
	}
	
	public double getValorLimite() {
		return valorLimite;
	}

	public void setValorLimite(double valorLimite) {
		this.valorLimite = valorLimite;
	}
	
}
