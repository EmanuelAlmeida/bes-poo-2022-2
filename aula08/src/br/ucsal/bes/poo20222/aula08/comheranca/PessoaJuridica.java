package br.ucsal.bes.poo20222.aula08.comheranca;

public final class PessoaJuridica extends Pessoa {

	private String cnpj;
	private String inscricaoEstadual;
	private String inscricaoMunicipal;

	public PessoaJuridica(String nome, String cnpj, String inscricaoEstadual, String inscricaoMunicipal) {
		super(nome);
		this.cnpj = cnpj;
		this.inscricaoEstadual = inscricaoEstadual;
		this.inscricaoMunicipal = inscricaoMunicipal;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}

	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}

	public String getInscricaoMunicipal() {
		return inscricaoMunicipal;
	}

	public void setInscricaoMunicipal(String inscricaoMunicipal) {
		this.inscricaoMunicipal = inscricaoMunicipal;
	}

}
