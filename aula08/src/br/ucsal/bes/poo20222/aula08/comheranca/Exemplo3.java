package br.ucsal.bes.poo20222.aula08.comheranca;

import java.util.ArrayList;
import java.util.List;

public class Exemplo3 {

	public static void main(String[] args) {

		List<String> nomes = new ArrayList<>();
		List<PessoaFisica> pessoasFisicas = new ArrayList<>();
		List<PessoaJuridica> pessoasJuridicas = new ArrayList<>();

		List<Pessoa> pessoas = new ArrayList<>();

		PessoaFisica pessoaFisica1 = new PessoaFisica("claudio");
		PessoaJuridica pessoaJuridica1 = new PessoaJuridica("Empresa x", "123123", "121433", "232345425");

		pessoas.add(pessoaFisica1);
		pessoas.add(pessoaJuridica1);

		Pessoa pessoa1 = pessoaFisica1; // UP CAST
		
		Pessoa pessoa2 = new PessoaFisica("Maria"); // UP CAST
		System.out.println(pessoa2.getNome());
		System.out.println(pessoa2.getEndereco());
		PessoaFisica pessoaFisica2 = (PessoaFisica) pessoa2;
		System.out.println(pessoaFisica2.getNomeMae());

		// pessoa1.getNomeMae(); // durante a compilação, os métodos que podem
		// ser chamados estão restritos aos do tipo da variável e NÃO da instância
		// por ela apontada.

		apresentar(pessoaFisica1);
		apresentar(pessoaJuridica1);

	}

	private static void apresentar(Pessoa pessoa) {
		System.out.println("\n*************************");
		System.out.println("pessoa.nome=" + pessoa.getNome());
		System.out.println("pessoa.endereco=" + pessoa.getEndereco());
		System.out.println("pessoa.telefones=" + pessoa.getTelefones());

//		if (pessoa instanceof PessoaFisica) {
//			PessoaFisica pessoaFisica = (PessoaFisica) pessoa;
//			System.out.println("pessoa.cpf=" + pessoaFisica.getCpf());
//			System.out.println("pessoa.nomeMae=" + pessoaFisica.getNomeMae());
//		}
		if (pessoa instanceof PessoaFisica pessoaFisica) {
			System.out.println("pessoa.cpf=" + pessoaFisica.getCpf());
			System.out.println("pessoa.nomeMae=" + pessoaFisica.getNomeMae());
		}
	}

}
