package br.ucsal.bes.poo20222.aula08.comheranca;

public class Exemplo4 {

	public static void main(String[] args) {

		// Não é possível instanciar classes com modificador abstract
		// Pessoa pessoa1 = new Pessoa("ana");

		PessoaFisica pessoaFisica1 = new PessoaFisica("claudio", "123", "maria");
		PessoaFisica pessoaFisica2 = new PessoaFisica("maria", "123", "maria");
		exibirPessoaFisica(pessoaFisica1);

		System.out.println("pessoaFisica1=" + pessoaFisica1.toString());
		System.out.println("pessoaFisica2=" + pessoaFisica2);

		System.out.println("pessoaFisica1==pessoaFisica2=" + (pessoaFisica1 == pessoaFisica2));
		System.out.println("pessoaFisica1.equals(pessoaFisica2)=" + pessoaFisica1.equals(pessoaFisica2));

	}

	private static void exibirPessoaFisica(PessoaFisica pessoaFisica) {
		
	}

}
