package br.ucsal.bes.poo20222.aula08.comheranca;

import java.util.List;

public class Enfermeiro extends PessoaFisica {

	private String coren;
	private List<String> especialidades;

	public Enfermeiro(String nome, String cpf, String nomeMae, String coren) {
		super(nome, cpf, nomeMae);
		this.coren = coren;
	}

	public String getCoren() {
		return coren;
	}

	public void setCoren(String coren) {
		this.coren = coren;
	}

	public List<String> getEspecialidades() {
		return especialidades;
	}

	public void setEspecialidades(List<String> especialidades) {
		this.especialidades = especialidades;
	}

}
