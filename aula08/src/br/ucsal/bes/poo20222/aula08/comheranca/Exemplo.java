package br.ucsal.bes.poo20222.aula08.comheranca;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Exemplo {

	public static void main(String[] args) {

		PessoaFisica pessoaFisica1 = new PessoaFisica("Claudio","123123","Maria");

		pessoaFisica1.setCpf("1312312");
		List<String> telefones = Arrays.asList("1231231", "23423423");
		pessoaFisica1.setTelefones(telefones);

		List<String> telefones2 = new ArrayList<>();
		telefones2.add("1231231");
		telefones2.add("23423423");

	}

}
