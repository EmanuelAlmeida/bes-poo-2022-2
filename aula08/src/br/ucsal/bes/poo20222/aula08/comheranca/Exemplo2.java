package br.ucsal.bes.poo20222.aula08.comheranca;

public class Exemplo2 {

	public static void main(String[] args) {

		Medico medico1 = new Medico("Claudio", "2234234456", "Maria", "123BA");
		System.out.println("medico1.nome=" + medico1.getNome());
		System.out.println("medico1.crm=" + medico1.getCrm());

		Pessoa pessoa2 = new Medico("Ana", "46746746", "Clara", "456BA");
		System.out.println("medico2.nome=" + pessoa2.getNome());
		// System.out.println("medico2.crm=" + pessoa2.getCrm());

		Medico medico2 = (Medico) pessoa2;
		System.out.println("medico2.nome=" + medico2.getNome());
		System.out.println("medico2.crm=" + medico2.getCrm());
		
	}

}
