package br.ucsal.bes.poo20222.aula08.comheranca;

import java.util.Objects;

public class PessoaFisica extends Pessoa {

	private String cpf;
	private String nomeMae;

	public PessoaFisica(String nome, String cpf, String nomeMae) {
		super(nome);
		this.cpf = cpf;
		this.nomeMae = nomeMae;
	}

	public PessoaFisica(String nome) {
		super(nome);
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNomeMae() {
		return nomeMae;
	}

	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(cpf, nomeMae);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PessoaFisica other = (PessoaFisica) obj;
		return Objects.equals(cpf, other.cpf) && Objects.equals(nomeMae, other.nomeMae);
	}

	@Override
	public String toString() {
		return "PessoaFisica [cpf=" + cpf + ", nomeMae=" + nomeMae + ", " + super.toString() + "]";
	}

}
