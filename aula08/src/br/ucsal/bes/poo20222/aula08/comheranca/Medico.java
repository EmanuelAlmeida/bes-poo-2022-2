package br.ucsal.bes.poo20222.aula08.comheranca;

import java.util.List;

public class Medico extends PessoaFisica {

	private String crm;
	private List<String> especialidades;

	public Medico(String nome, String cpf, String nomeMae, String crm) {
		super(nome, cpf, nomeMae);
		System.out.println("nome=" + this.getNome());
		this.crm = crm;
	}

	public String getCrm() {
		return crm;
	}

	public void setCrm(String crm) {
		this.crm = crm;
	}

	public List<String> getEspecialidades() {
		return especialidades;
	}

	public void setEspecialidades(List<String> especialidades) {
		this.especialidades = especialidades;
	}

}
