package br.ucsal.bes.poo20222.aula08.semheranca.classesindividuais;

import java.util.ArrayList;
import java.util.List;

// NÃO FAÇA ASSIM!!!!
public class PessoaFisica {

	private String cpf;
	private String nome;
	private String endereco;
	private List<String> telefones;
	private String nomeMae;

	public PessoaFisica(String cpf, String nome, String endereco, List<String> telefones, String nomeMae) {
		super();
		this.cpf = cpf;
		this.nome = nome;
		this.endereco = endereco;
		this.telefones = telefones;
		this.nomeMae = nomeMae;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public List<String> getTelefones() {
		return new ArrayList<>(telefones);
	}

	public void setTelefones(List<String> telefones) {
		this.telefones = telefones;
	}

	public String getNomeMae() {
		return nomeMae;
	}

	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}

}
