package br.ucsal.bes.poo20222.aula08.semheranca.classesindividuais;

import java.util.ArrayList;
import java.util.List;

public class Exemplo {

	public static void main(String[] args) {
		
		List<PessoaFisica> pessoasFisicas = new ArrayList<>();
		List<PessoaJuridica> pessoasJuridicas = new ArrayList<>();
		
		// Isso NÃO existe:
		//List<PessoaFisica,PessoaJuridica> pessoas = new ArrayList<>();
		
	}
	
}
