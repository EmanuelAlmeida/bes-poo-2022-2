package br.ucsal.bes.poo20222.aula08.semheranca.classeunica;

import java.util.ArrayList;
import java.util.List;

public class Exemplo {

	public static void main(String[] args) {

		List<Pessoa> pessoas = new ArrayList<>();

		// Gambiarra!!!!!
		for (Pessoa pessoa : pessoas) {
			if (pessoa.getCnpj() != null) {
				// aqui temos uma pessoa jurídica
			} else {
				// aqui temos uma pessoa física
			}
		}

		// List<PessoaJuridicas> pessoasJuridicas = new ArrayList<>();
		// List<PessoaFisica> pessoasFisica = new ArrayList<>();

		
		Pessoa pessoa = new Pessoa("123123123", null, null, "Claudio", null, null, null, "Maria");
		
		
	}

}
