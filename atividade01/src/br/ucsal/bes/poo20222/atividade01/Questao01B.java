package br.ucsal.bes.poo20222.atividade01;

import java.util.Scanner;

// CTRL + 1 = corrigir problemas

public class Questao01B {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		obterNotaCalcularExibirConceito();
	}

	private static void obterNotaCalcularExibirConceito() {
		int nota = obterNota();
		String conceito = calcularConceito(nota);
		exibirConceito(conceito);
	}

	private static void exibirConceito(String conceito) {
		// Saída
		System.out.println("O conceito é " + conceito + ".");
	}

	private static String calcularConceito(int nota) {
		// Processamento - definição do conceito
		String conceito;
		if (nota <= 49) {
			conceito = "Insuficiente";
		} else if (nota <= 64) {
			conceito = "Regular";
		} else if (nota <= 84) {
			conceito = "Bom";
		} else {
			conceito = "Ótimo";
		}
		return conceito;
	}

	private static int obterNota() {
		// Entrada de dados - a nota
		System.out.println("Informa a nota (0 a 100, intervalo fechado):");
		int nota = scanner.nextInt();
		return nota;
	}

}
