package br.ucsal.bes.poo20222.atividade01;

import java.util.Scanner;

//@formmater:off
/**
 * 
 * Crie um algoritmo que peça nome, altura e peso de 10 pessoas e apresente:
 * 
 * Nome e peso da mais pesada;
 * 
 * Nome e altura da mais alta.
 * 
 * @author antoniocp
 *
 */
public class Questao04 {

	private static final int QTD_PESSOAS = 3;

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		obterPessoasCalcularMaisPesadaMaisAlta();
	}

	private static void obterPessoasCalcularMaisPesadaMaisAlta() {
		Pessoa[] pessoas = new Pessoa[QTD_PESSOAS];
		Pessoa pessoaMaisPesada;
		Pessoa pessoaMaisAlta;

		obterPessoas(pessoas);

//		pessoaMaisPesada = calcularPessoaMaisPesada();
//		pessoaMaisAlta = calcularPessoaMaisAlta();
//
//		exibirPessoaMaisPesada(pessoaMaisPesada);
//		exibirPessoaMaisAlta(pessoaMaisAlta);
	}

	private static void obterPessoas(Pessoa[] pessoas) {
		System.out.println("Informe " + pessoas.length + " pessoas:");
		for (int i = 0; i < pessoas.length; i++) {
			System.out.println("Pessoa " + (i + 1) + ":");
			pessoas[i] = obterPessoa();
			System.out.println();
		}
	}

	private static Pessoa obterPessoa() {
		Pessoa pessoa = new Pessoa();

		System.out.print("Nome:");
		pessoa.nome = scanner.nextLine();
		
		System.out.print("Altura:");
		pessoa.altura = scanner.nextDouble();
		scanner.nextLine();
		
		System.out.print("Peso:");
		pessoa.peso = scanner.nextDouble();
		scanner.nextLine();
		
		return pessoa;
	}

}
