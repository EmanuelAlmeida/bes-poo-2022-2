package br.ucsal.bes.poo20222.atividade01;

import java.util.Scanner;

// @formmater:off
/*
Suponha que o conceito de um aluno seja determinado em função da sua nota. Suponha, também, que esta nota seja um valor inteiro na faixa de 0 a 100 (intervalo fechado), conforme a seguinte faixa:

Nota		Conceito
0 a 49		Insuficiente
50 a 64		Regular
65 a 84		Bom
85 a 100	Ótimo

Crie um programa em Java que leia a nota de um aluno e apresente o conceito do mesmo.
Não é necessário tratar valores fora da faixa.
 */
//@formmater:on

// CTRL + SHIFT + F

public class Questao01 {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		
		// Entrada de dados - a nota
		System.out.println("Informa a nota (0 a 100, intervalo fechado):");
		int nota = scanner.nextInt();

		// Processamento - definição do conceito
		String conceito;
		if (nota <= 49) {
			conceito = "Insuficiente";
		} else if (nota <= 64) {
			conceito = "Regular";
		} else if (nota <= 84) {
			conceito = "Bom";
		} else {
			conceito = "Ótimo";
		}

		// Saída
		System.out.println("O conceito é " + conceito + ".");

	}

}
