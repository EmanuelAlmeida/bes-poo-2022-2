package br.ucsal.bes.poo20222.atividade02.enums;

public enum OpcaoMenuEnum {

	INCLUIR_PESSOA(1, "Incluir pessoa"),

	INCLUIR_VEICULO(2, "Incluir veículo"),

	LISTAR_VEICULOS(3, "Listar veículos"),

	LISTAR_PESSOAS(4, "Listar pessoas"),
	
	PESQUISAR_PESSOA_POR_CPF(5, "Pesquisar pessoa por cpf"),

	SAIR(9, "Sair");

	private int codigo;
	private String descricao;

	private OpcaoMenuEnum(int codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}


	public int getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}
	
	public static OpcaoMenuEnum valueOfCodigo(int codigo) {
		for (OpcaoMenuEnum opcao : values()) {
			if (codigo == opcao.getCodigo()) {
				return opcao;
			}
		}
		// FIXME Mudar de "return null" para exception quando o assunto exceções for abordado.
		return null;
	}

}
