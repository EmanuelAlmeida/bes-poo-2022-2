package br.ucsal.bes.poo20222.atividade02.exception;

import java.util.ArrayList;
import java.util.List;

// Exceção "turbinada".
public class Negocio2Exception extends Exception {

	private static final long serialVersionUID = 1L;

	private final List<String> messages = new ArrayList<>();

	public Negocio2Exception() {
	}

	public Negocio2Exception(String message) {
		addMessage(message);
	}

	public void addMessage(String message) {
		messages.add(message);
	}

	@Override
	public String getMessage() {
		return messages.size() == 1 ? messages.get(0) : messages.toString();
	}

	public List<String> getMessages() {
		return new ArrayList<>(messages);
	}

	public boolean isExisteMessage() {
		return !messages.isEmpty();
	}
	
}
