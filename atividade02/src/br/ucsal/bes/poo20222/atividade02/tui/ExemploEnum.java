package br.ucsal.bes.poo20222.atividade02.tui;

import java.util.Scanner;

import br.ucsal.bes.poo20222.atividade02.enums.OpcaoMenuEnum;

public class ExemploEnum {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		for (OpcaoMenuEnum opcao : OpcaoMenuEnum.values()) {
			System.out.println(opcao);
		}
		System.out.println("Informe uma opção:");
		String opcaoString = scanner.nextLine();

		OpcaoMenuEnum opcaoSelecionada = OpcaoMenuEnum.valueOf(opcaoString.toUpperCase());
		System.out.println("opcaoSelecionada=" + opcaoSelecionada.getCodigo() + "|" + opcaoSelecionada.getDescricao());
	}

}
