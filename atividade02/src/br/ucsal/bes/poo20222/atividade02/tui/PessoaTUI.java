package br.ucsal.bes.poo20222.atividade02.tui;

import br.ucsal.bes.poo20222.atividade02.business.PessoaBO;
import br.ucsal.bes.poo20222.atividade02.domain.Endereco;
import br.ucsal.bes.poo20222.atividade02.domain.Pessoa;
import br.ucsal.bes.poo20222.atividade02.exception.LadraoException;
import br.ucsal.bes.poo20222.atividade02.exception.NegocioException;

public class PessoaTUI {

	private PessoaTUI() {
	}

	public static void incluir() {
		String cpf = TUIUtil.obterString("Informe o cpf:");
		String nome = TUIUtil.obterString("Informe o nome:");
		String telefone = TUIUtil.obterString("Informe o telefone:");

		try {
			Pessoa pessoa = new Pessoa(cpf, nome, new Endereco());
			pessoa.addTelefone(telefone);
			PessoaBO.incluir(pessoa);
			System.out.println("Pessoa incluída com sucesso!");
		} catch (NegocioException e) {
			System.out.println("Pessoa não válida: " + e.getMessage());
		} catch (LadraoException e) {
			// aqui chamar a polícia
		}
	}

	public static void pesquisarPorCpf() {
		String cpf = TUIUtil.obterString("Informe o cpf:");
		try {
			Pessoa pessoa = PessoaBO.encontrarPorCpf(cpf);
			System.out.println("Nome:" + pessoa.getNome());
			System.out.println("Endereço:" + pessoa.getEndereco());
			System.out.println("Telefones:" + pessoa.getTelefones());
		} catch (NegocioException e) {
			System.out.println(e.getMessage());
		}
	}

}
