package br.ucsal.bes.poo20222.atividade02.tui;

import br.ucsal.bes.poo20222.atividade02.enums.OpcaoMenuEnum;

public class MenuTUI {

	public static void executar() {
		OpcaoMenuEnum opcao;
		do {
			exibirOpcoes();
			opcao = obterOpcaoDeseja();
			executarOpcao(opcao);
		} while (opcao != OpcaoMenuEnum.SAIR);
	}

	private static void executarOpcao(OpcaoMenuEnum opcao) {
		switch (opcao) {
		case INCLUIR_PESSOA:
			PessoaTUI.incluir();
			break;
		case INCLUIR_VEICULO:
			System.out.println("Inclusão de veículos - FUNCIONALIDADE NÃO IMPLEMENTADA");
			break;
		case LISTAR_VEICULOS:
			System.out.println("Listagem de veículos - FUNCIONALIDADE NÃO IMPLEMENTADA");
			break;
		case LISTAR_PESSOAS:
			System.out.println("Listagem de pessoas - FUNCIONALIDADE NÃO IMPLEMENTADA");
			break;
		case PESQUISAR_PESSOA_POR_CPF:
			PessoaTUI.pesquisarPorCpf();
			break;
		case SAIR:
			System.out.println("Bye...");
			break;
		default:
			System.out.println("Opção não válida.");
		}
	}

	private static OpcaoMenuEnum obterOpcaoDeseja() {
		int opcaoInt = TUIUtil.obterInt("Informe o código da opção desejada:");
		return OpcaoMenuEnum.valueOfCodigo(opcaoInt);
	}

	private static void exibirOpcoes() {
		for (OpcaoMenuEnum opcao : OpcaoMenuEnum.values()) {
			System.out.println(opcao.getCodigo() + " - " + opcao.getDescricao());
		}
	}

}
