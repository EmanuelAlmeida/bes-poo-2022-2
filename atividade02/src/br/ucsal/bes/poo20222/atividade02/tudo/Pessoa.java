package br.ucsal.bes.poo20222.atividade02.tudo;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes.poo20222.atividade02.domain.Endereco;
import br.ucsal.bes.poo20222.atividade02.exception.LadraoException;
import br.ucsal.bes.poo20222.atividade02.exception.Negocio2Exception;
import br.ucsal.bes.poo20222.atividade02.exception.NegocioException;
import br.ucsal.bes.poo20222.atividade02.persistence.FileUtil;
import br.ucsal.bes.poo20222.atividade02.tui.TUIUtil;

public class Pessoa {
	
	// 1. Constantes
	// 2. Atributos
	// 3. Construtores
	// 4.1. Métodos públicos
	// 4.2. Métodos privados
	// 5. equals/hashCode/toString (método público, geralmente ele fica no final da
	// classe)

	private static final int TAM_CPF = 11;

	private static long cont = 0;

	private static List<Pessoa> pessoas = new ArrayList<>();
	
	private long id = 0;

	private String cpf;

	private String nome;

	private Endereco endereco;

	private List<String> telefones = new ArrayList<>();

	public Pessoa() {
		definirId();
	}

	public Pessoa(String cpf, String nome) throws NegocioException {
		this();
		setCpf(cpf);
		setNome(nome);
	}

	public Pessoa(String cpf, String nome, Endereco endereco) throws NegocioException {
		this(cpf, nome);
		this.endereco = endereco;
	}

	public Long getId() {
		return id;
	}

	public Pessoa(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public List<String> getTelefones() {
		return telefones;
//		return new ArrayList<>(telefones);
//		return Arrays.copyOf(telefones, QTD_MAX_TELEFONES);
	}

	public void addTelefone(String telefone) {
		telefones.add(telefone);
	}

	public void setCpf(String cpf) throws NegocioException {
		isValido(cpf);
		this.cpf = cpf;
	}

	public String getCpf() {
		return cpf;// formartarCpf();
	}

	// FIXME Validação deve trabalha com exceção.
	private void isValido(String novoCpf) throws NegocioException {
		if(novoCpf == null || novoCpf.trim().length() != TAM_CPF) {
			throw new NegocioException("CPF não válido");
		}
	}

	private boolean isValido(int idade) {
		return true;
	}

	private String formartarCpf() {
		if (cpf == null || cpf.length() < TAM_CPF) {
			return cpf;
		}
		return cpf.substring(0, 3) + "." + cpf.substring(3, 6) + "." + cpf.substring(6, 9) + "-" + cpf.substring(9, 11);
	}

	private void definirId() {
		Pessoa.cont++;
		this.id = Pessoa.cont;
	}

	@Override
	public String toString() {
		return "Pessoa [id=" + id + ", cpf=" + cpf + ", nome=" + nome + ", endereco=" + endereco + ", telefones="
				+ telefones + "]";
	}

	public static void incluirTUI() {
		String cpf = TUIUtil.obterString("Informe o cpf:");
		String nome = TUIUtil.obterString("Informe o nome:");
		String telefone = TUIUtil.obterString("Informe o telefone:");

		try {
			Pessoa pessoa = new Pessoa(cpf, nome, new Endereco());
			pessoa.addTelefone(telefone);
			incluirBO(pessoa);
			System.out.println("Pessoa incluída com sucesso!");
		} catch (NegocioException e) {
			System.out.println("Pessoa não válida: " + e.getMessage());
		} catch (LadraoException e) {
			// aqui chamar a polícia
		}
	}

	public static void pesquisarPorCpfTUI() {
		String cpf = TUIUtil.obterString("Informe o cpf:");
		try {
			Pessoa pessoa = encontrarPorCpf(cpf);
			System.out.println("Nome:" + pessoa.getNome());
			System.out.println("Endereço:" + pessoa.getEndereco());
			System.out.println("Telefones:" + pessoa.getTelefones());
		} catch (NegocioException e) {
			System.out.println(e.getMessage());
		}
	}

	public static void incluirBO(Pessoa pessoa) throws NegocioException, LadraoException {
		validar(pessoa);
		pessoa.setNome(pessoa.getNome().toUpperCase());
		incluirDAO(pessoa);
	}

	public static Pessoa encontrarPorCpf(String cpf) throws NegocioException {
		Pessoa pessoa = encontrarPorCpf(cpf);
		// aqui posso ter outros comandos....
		return pessoa;
	}

	private static void validar(Pessoa pessoa) throws NegocioException, LadraoException {
		if (pessoa.getNome() == null || pessoa.getNome().isBlank()) {
			throw new NegocioException("Nome não informado.");
		}
		if (pessoa.getCpf() == null || pessoa.getCpf().isBlank()) {
			throw new NegocioException("CPF não informado.");
		}
		// vc aciona um serviço Web e descobre o usuário é ladrão!
		boolean isLadrao = false;
		if (isLadrao) {
			throw new LadraoException("Essa pessoa tem q ir pra cadeia.");
		}
	}

	private static void validar2(Pessoa pessoa) throws Negocio2Exception {
		Negocio2Exception e = new Negocio2Exception();
		if (pessoa.getNome() == null || pessoa.getNome().isBlank()) {
			e.addMessage("Nome não informado.");
		}
		if (pessoa.getCpf() == null || pessoa.getCpf().isBlank()) {
			e.addMessage("CPF não informado.");
		}
		if(e.isExisteMessage()) {
			throw e;
		}
	}

	public static void incluirDAO(Pessoa pessoa) {
		pessoas.add(pessoa);
		FileUtil.toDisk(toCsv(pessoas));
	}

	public static Pessoa encontrarPorCpfDAO(String cpf) throws NegocioException {
		for (Pessoa pessoa : pessoas) {
			if (cpf.equals(pessoa.getCpf())) {
				return pessoa;
			}
		}
		throw new NegocioException("Nenhuma pessoa encontrada para o CPF especificado.");
	}

	public static String toCsv(List<Pessoa> pessoas) {
//		String csv = "";
//		for (Pessoa pessoa : pessoas) {
//			csv += toCsv(pessoa) + System.getProperty("line-break");
//		}
//		return csv;		
		return null;
	}

//	private static String toCsv(Pessoa pessoa) {
//		return pessoa.getId() + "," + pessoa.getCpf() + "," + pessoa.getNome() + ",";
//	}
	
}
