package br.ucsal.bes.poo20222.atividade02.business;

import br.ucsal.bes.poo20222.atividade02.domain.Pessoa;
import br.ucsal.bes.poo20222.atividade02.exception.LadraoException;
import br.ucsal.bes.poo20222.atividade02.exception.Negocio2Exception;
import br.ucsal.bes.poo20222.atividade02.exception.NegocioException;
import br.ucsal.bes.poo20222.atividade02.persistence.PessoaDAO;

public class PessoaBO {

	public static void incluir(Pessoa pessoa) throws NegocioException, LadraoException {
		validar(pessoa);
		pessoa.setNome(pessoa.getNome().toUpperCase());
		PessoaDAO.incluir(pessoa);
	}

	public static Pessoa encontrarPorCpf(String cpf) throws NegocioException {
		Pessoa pessoa = PessoaDAO.encontrarPorCpf(cpf);
		// aqui posso ter outros comandos....
		return pessoa;
	}

	private static void validar(Pessoa pessoa) throws NegocioException, LadraoException {
		if (pessoa.getNome() == null || pessoa.getNome().isBlank()) {
			throw new NegocioException("Nome não informado.");
		}
		if (pessoa.getCpf() == null || pessoa.getCpf().isBlank()) {
			throw new NegocioException("CPF não informado.");
		}
		// vc aciona um serviço Web e descobre o usuário é ladrão!
		boolean isLadrao = false;
		if (isLadrao) {
			throw new LadraoException("Essa pessoa tem q ir pra cadeia.");
		}
	}

	private static void validar2(Pessoa pessoa) throws Negocio2Exception {
		Negocio2Exception e = new Negocio2Exception();
		if (pessoa.getNome() == null || pessoa.getNome().isBlank()) {
			e.addMessage("Nome não informado.");
		}
		if (pessoa.getCpf() == null || pessoa.getCpf().isBlank()) {
			e.addMessage("CPF não informado.");
		}
		if(e.isExisteMessage()) {
			throw e;
		}
	}
}
