package br.ucsal.bes.poo20222.atividade02.domain;

import java.util.ArrayList;
import java.util.List;

public class Exemplo1 {

	public static void main(String[] args) {
		
		fazerCoisasComUmaLista(new ArrayList<>());
		
	}

	private static void fazerCoisasComUmaLista(List<String> nomes) {
		nomes.add("antonio");
		nomes.add("claudio");
		nomes.add("neiva");
		
		System.out.println(nomes);

		nomes.set(1, "pedreira");

		System.out.println(nomes);
		
		System.out.println(nomes.get(0));

		System.out.println(nomes.size());

		nomes.remove(2);
		
		System.out.println(nomes);
		
		System.out.println(nomes.size());

		System.out.println("Nomes (1 por linha):");
		for(String nome: nomes) {
			System.out.println(nome);
		}
		
		nomes.clear();
		
		System.out.println(nomes.size());
	}

}
