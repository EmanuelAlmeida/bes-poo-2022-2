package br.ucsal.bes.poo20222.atividade02.domain;

public class Endereco {

	private String cep;

	private String logradouro;

	private String numero;

	private String complemento;

	private String bairro;

	public Endereco() {
		System.out.println("fiz alguma coisa no construtor vazio");
	}
	
	public Endereco(String cep) {
		this();
		System.out.println("fiz alguma coisa no construtor (string)");
		setCep(cep);
		// cria um código que vai até os Correios e busca o logradouro a partir do cep
		// fizer mais código aqui o outro construto vai chamar
	}

	public Endereco(String cep, String numero) {
		this(cep);
		System.out.println("fiz alguma coisa no construtor (string, string)");
		this.numero = numero;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getLogradouro() {
		return logradouro;
	}

	private void setCep(String cep) {
		if (cep == null || cep.isEmpty()) {
			throw new RuntimeException("Não é permitido construir Endereco sem especificar o cep");
		}
		this.cep = cep;
	}

}
