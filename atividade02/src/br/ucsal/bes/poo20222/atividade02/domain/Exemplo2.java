package br.ucsal.bes.poo20222.atividade02.domain;

public class Exemplo2 {

	public static void main(String[] args) {

		Pessoa pessoaA = new Pessoa();
		Pessoa pessoaB = new Pessoa();
		Pessoa pessoaC = new Pessoa();

		System.out.println("pessoaA.id=" + pessoaA.getId()); // pessoaA.id=1
		System.out.println("pessoaB.id=" + pessoaB.getId()); // pessoaB.id=2
		System.out.println("pessoaC.id=" + pessoaC.getId()); // pessoaC.id=3

	}

}
