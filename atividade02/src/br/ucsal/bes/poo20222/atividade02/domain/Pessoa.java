package br.ucsal.bes.poo20222.atividade02.domain;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes.poo20222.atividade02.exception.NegocioException;

public class Pessoa {

	// 1. Constantes
	// 2. Atributos
	// 3. Construtores
	// 4.1. Métodos públicos
	// 4.2. Métodos privados
	// 5. equals/hashCode/toString (método público, geralmente ele fica no final da
	// classe)

	private static final int TAM_CPF = 11;

	private static long cont = 0;

	private long id = 0;

	private String cpf;

	private String nome;

	private Endereco endereco;

	private List<String> telefones = new ArrayList<>();

	public Pessoa() {
		definirId();
	}

	public Pessoa(String cpf, String nome) throws NegocioException {
		this();
		setCpf(cpf);
		setNome(nome);
	}

	public Pessoa(String cpf, String nome, Endereco endereco) throws NegocioException {
		this(cpf, nome);
		this.endereco = endereco;
	}

	public Long getId() {
		return id;
	}

	public Pessoa(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public List<String> getTelefones() {
		return telefones;
//		return new ArrayList<>(telefones);
//		return Arrays.copyOf(telefones, QTD_MAX_TELEFONES);
	}

	public void addTelefone(String telefone) {
		telefones.add(telefone);
	}

	public void setCpf(String cpf) throws NegocioException {
		isValido(cpf);
		this.cpf = cpf;
	}

	public String getCpf() {
		return cpf;// formartarCpf();
	}

	// FIXME Validação deve trabalha com exceção.
	private void isValido(String novoCpf) throws NegocioException {
		if(novoCpf == null || novoCpf.trim().length() != TAM_CPF) {
			throw new NegocioException("CPF não válido");
		}
	}

	private boolean isValido(int idade) {
		return true;
	}

	private String formartarCpf() {
		if (cpf == null || cpf.length() < TAM_CPF) {
			return cpf;
		}
		return cpf.substring(0, 3) + "." + cpf.substring(3, 6) + "." + cpf.substring(6, 9) + "-" + cpf.substring(9, 11);
	}

	private void definirId() {
		Pessoa.cont++;
		this.id = Pessoa.cont;
	}

	@Override
	public String toString() {
		return "Pessoa [id=" + id + ", cpf=" + cpf + ", nome=" + nome + ", endereco=" + endereco + ", telefones="
				+ telefones + "]";
	}

}
